<?php

$auth = array(
    'table'             => 'user',
    'fields'            => array(
        'email',
        'password'
    ),
    'encryption_fields' => array(
        'password'
    ),
    'encryption_key'    => ''
);

return $auth;

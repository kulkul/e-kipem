<?php

class Resident_type_model extends MY_Model {

    var $_table = 'resident_type';

    var $has_many = array('resident');

    public function __construct () {
        parent::__construct();
    }
}

<?php

class Resident_model extends MY_Model {

    var $_table = 'resident';

    public function __construct () {
        parent::__construct();
    }

    public function sex_dropdown () {

        return array(
            '0' => 'Perempuan',
            '1' => 'Laki-laki'
        );

    }

    public function religion_dropdown () {

        $rel = array(
            'BUDHA','HINDU','ISLAM','KRISTEN','KATOLIK','KONGHUCHU','LAINNYA'
        );

        return array_combine($rel, $rel);
    }

    public function marital_dropdown () {

        return array(
            '0' => 'Belum Kawin',
            '1' => 'Kawin',
            '2' => 'Cerai'
        );

    }
}

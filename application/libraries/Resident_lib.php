<?php

class Resident_lib {

    private $db;
    private $ci;
    private $errors;
    private $validation;

    public function __construct () {
        $this->ci = &get_instance();

        $this->ci->load->model(array(
            'resident_model'
        ));

        $this->ci->load->library(array(
            'form_validation'
        ));

        $this->validation = $this->ci->form_validation;
    }

    public function validate ($data) {
        $this->validation->set_data($data);

        $this->validation->set_rules('name', 'Name', 'required');
        $this->validation->set_rules('nik', 'NIK', 'required');
        $this->validation->set_rules('birthplace', 'Birthplace', 'required');
        $this->validation->set_rules('birthdate', 'Birthdate', 'required');
        $this->validation->set_rules('sex', 'Sex', 'required');
        $this->validation->set_rules('religion', 'Religion', 'required');
        $this->validation->set_rules('marital_status', 'Marital Status', 'required');
        $this->validation->set_rules('occupation', 'Occupation', 'required');
        $this->validation->set_rules('sex', 'Sex', 'required');
        $this->validation->set_rules('nationality', 'Nationality', 'required');

        /* KTP SESSION */
        $this->validation->set_rules('ori_address', 'Origin Address', 'required');
        $this->validation->set_rules('ori_location_code', 'Origin Location', 'required');
        $this->validation->set_rules('valid_until', 'Valid Until', 'required');

        /* KOST SESSION */
        $this->validation->set_rules('cur_address', 'Current Address', 'required');
        $this->validation->set_rules('cur_location_code', 'Current Location', 'required');
        $this->validation->set_rules('resident_type_id', 'Resident Type', 'required');
        $this->validation->set_rules('validity', 'Validity');
        $this->validation->set_rules('exp_date', 'Exp Date');
        $this->validation->set_rules('photo', 'Photo');

        /* IBU KOST SESSION */
        $this->validation->set_rules('pic', 'Person in Charge (PIC)', 'required');
        $this->validation->set_rules('pic_address', 'PIC Address');
        $this->validation->set_rules('pic_phone', 'PIC Phone', 'required');
        $this->validation->set_rules('pic_email', 'PIC Email');

        if ($this->validation->run() == FALSE) {
            $this->errors = $this->validation->error_array();
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function insert ($data) {
        $data = $this->_apply_data($data);
        if ($this->validate($data)) {
            return $this->ci->resident_model->insert($data);
        } else {
            return false;
        }
    }

    public function update ($id, $data) {
        $data = $this->_apply_data($data);
        if ($this->validate($data)) {
            return $this->ci->resident_model->update($id, $data);
        } else {
            return false;
        }
    }

    public function validation_errors () {
        return $this->errors;
    }

    private function _apply_data ($data) {
        $data = array(
            'name'          => $data['name'],
            'nik'           => $data['nik'],
            'birthplace'    => $data['birthplace'],
            'birthdate'     => $data['birthdate'],
            'sex'           => $data['sex'],
            'ori_address'   => $data['ori_address'],
            'ori_location_code' => $data['ori_location_code'],
            'religion'      => $data['religion'],
            'marital_status' => $data['marital_status'],
            'occupation'    => $data['occupation'],
            'valid_until'   => $data['valid_until'],
            'nationality'   => $data['nationality'],
            'cur_address'   => $data['cur_address'],
            'cur_location_code'  => $data['cur_location_code'],
            'resident_type_id' => $data['resident_type_id'],
            'validity'      => $data['validity'],
            'exp_date'      => $data['exp_date'],
            'photo'         => $data['photo'],
            'pic'           => $data['pic'],
            'pic_address'   => $data['pic_address'],
            'pic_phone'     => $data['pic_phone'],
            'pic_email'     => $data['pic_email']
        );
        return $data;
    }
}

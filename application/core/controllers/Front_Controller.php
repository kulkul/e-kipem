<?php
if(!defined('BASEPATH')) exit('No direct script access allowed!');

class Front_Controller extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->__init();
    }

    public function __init () {
        /* the data */
        $this->stencil->title('eKipem');

        /* load style on controller */
        $this->stencil->css(array(
            'front/bootstrap.css',
            'ekipem.css',
            'animate.min.css',
            'font-awesome.css',
            'owl.carousel.css',
            'flexslider.css',
            'fancySelect.css',
            'responsive.css',
            'http://fonts.googleapis.com/css?family=Open+Sans:400,700',
            'http://fonts.googleapis.com/css?family=Belgrano'
        ));

        /* load script on controller */
        $this->stencil->js(array(
            'https://maps.googleapis.com/maps/api/js?sensor=false',
            'jQuery-2.1.3.min.js',
            'front/bootstrap.min.js',
            'jquery.scrollTo.js',
            'jquery.nav.js',
            'owl.carousel.min.js',
            'jquery.flexslider.js',
            'jquery.accordion.js',
            'jquery.placeholder.js',
            'jquery.fitvids.js',
            'gmap3.js',
            'fancySelect.js',
            'ekipem.js'
        ));

        /* the layout */
        $this->stencil->layout('front/landing-layout.php');

        /* the slices */
        $this->stencil->slice(array(
            'footer'=>'front/footer',
            'header'=>'front/header'
        ));
    }

}

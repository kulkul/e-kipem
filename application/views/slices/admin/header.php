<?php
    $CI = &get_instance();
    $user = $CI->kulkul_auth->user();
?>
<header class="main-header">
<a href="<?php echo site_url('admin'); ?>" class="logo"><?php echo config('site', 'name'); ?></a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top" role="navigation">
  <!-- Sidebar toggle button-->
  <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </a>
  <div class="navbar-custom-menu">
    <ul class="nav navbar-nav">
      <!-- User Account: style can be found in dropdown.less -->
      <li class="">
        <a href="<?php echo site_url('user/logout'); ?>?redirect=admin">Logout</a>
     </li>
    </ul>
  </div>
</nav>
</header>

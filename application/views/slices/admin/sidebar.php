<?php
    $CI =&get_instance();
    $user = $CI->kulkul_auth->user();
?>
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <!--div class="pull-left image">
        <img src="<?php echo asset_url('img/user2-160x160.jpg') ?>" class="img-circle" alt="User Image" />
    </div-->
      <div class="pull-left info">
        <p><?php echo $user['name']; ?></p>

        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">MAIN NAVIGATION</li>
      <li><a href="<?php echo site_url('admin'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>

      <li class="treeview">
      	<a href="#">
          <i class="glyphicon glyphicon-map-marker"></i> <span>Lokasi</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
        	<li><a href="<?php echo admin_url('location'); ?>"><i class="fa fa-circle-o"></i> Semua Lokasi</a></li>
        </ul>
      </li>

      <li class="treeview">
        <a href="#">
          <i class="fa fa-users"></i> <span>Data Pendatang</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo admin_url('resident'); ?>"><i class="fa fa-circle-o"></i> Lihat Data</a></li>
          <li><a href="<?php echo admin_url('resident/form'); ?>"><i class="fa fa-circle-o"></i> Tambah Data</a></li>
        </ul>
      </li>


      <li class="treeview">
        <a href="#">
          <i class="glyphicon glyphicon-folder-close"></i> <span>Data Master</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo admin_url('resident_type'); ?>"><i class="fa fa-circle-o"></i> Data Tipe Penduduk</a></li>
          <li><a href="<?php echo admin_url('user'); ?>"><i class="fa fa-circle-o"></i> User</a></li>
          <li><a href="<?php echo admin_url('user_type'); ?>"><i class="fa fa-circle-o"></i> Tipe User</a></li>
        </ul>
      </li>

    </ul>
  </section>
  <!-- /.sidebar -->
</aside>

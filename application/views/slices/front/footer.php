<?php if(!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div id="map_canvas"></div>		
<div class="footer">
	<div class="container box">

		<section id="info" class="row">
			<div class="col-sm-8">

				<!--== Social Icons ==-->
				<ul class="social-icons">
					<li><a href="#" class="fa fa-facebook"></a></li>
					<li><a href="#" class="fa fa-twitter"></a></li>
					<li><a href="#" class="fa fa-youtube"></a></li>
					<li><a href="#" class="fa fa-instagram"></a></li>
				</ul>

			</div>
			<div class="col-sm-4 text-right">
				<div class="copyrights">
					Build with <span class="fa fa-heart"></span> by <a href="http://kulkul.id" target="_blank">KULKUL.ID</a> // All Rights Reserved &copy; 2015
				</div>
			</div>
		</section>

	</div>
</div>
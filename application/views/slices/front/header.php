<?php if(!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php $CI = &get_instance(); ?>
<div id="menu" class="header-menu fixed">
    <div class="container box">
        <div class="row">
            <nav role="navigation" class="col-sm-12">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!--== Logo ==-->
                    <span class="navbar-brand logo">
                        <span class="fa fa-credit-card"></span><?php echo $judul_web; ?>
                    </span>

                </div>
                <div class="navbar-collapse collapse">

                    <!--== Navigation Menu ==-->
                    <ul class="nav navbar-nav">
                        <li class="current"><a href="#header">Beranda</a></li>
                        <li><a href="#langkahmudah">4 Langkah Mudah</a></li>
                        <li><a href="#pengenalan">Mengapa e-Kipem?</a></li>
                        <li><a href="#cekstatus">Cek Status</a></li>
                        <li><a href="#register">Daftar e-Kipem</a></li>
                        <li><a href="<?php echo site_url("admin"); ?>" class="withlink" target="_blank">Login</a></li>
                    </ul>

                </div>
            </nav>
        </div>
    </div>
</div>

<!--== Site Description ==-->
<div class="header-cta">
    <div class="container">
        <div class="row">
            <div class="blk">
                <h1><?php echo $tagline_web;?></h1>
            </div>
        </div>
        <div class="row">
            <div class="blk">
                <input type="submit" value="Daftar Sekarang" name="submit" id="goregister">
            </div>
        </div>
    </div>
</div>

<div class="header-bg">
    <div id="preloader">
        <div class="preloader"></div>
    </div>
    <div class="main-slider" id="main-slider">

        <!--== Main Slider ==-->
        <ul class="slides">
            <li><img src="<?php echo base_url("/assets/images/slider/bg-slide-01.jpg");?>" alt="1"/></li>
            <li><img src="<?php echo base_url("/assets/images/slider/bg-slide-02.jpg");?>" alt="2"/></li>
        </ul>

    </div>
</div>
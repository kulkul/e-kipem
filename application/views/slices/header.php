<?php if(!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php $CI = &get_instance(); ?>
<nav class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo site_url(); ?>">e-Kipem</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="<?php echo site_url(); ?>">Beranda</a></li>
                <li><a href="<?php echo site_url(); ?>">Daftar</a></li>
                <li><a href="<?php echo site_url(); ?>">Cek Status</a></li>
                <li><a href="<?php echo site_url(); ?>">Admin</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div>
</nav>

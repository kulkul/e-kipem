<table class="table">
    <thead>
        <tr>
            <th></th>
            <th>Desa</th>
            <th>Kecamatan</th>
            <th>Kabupaten</th>
            <th>Provinsi</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($villages as $key => $village): ?>
        <?php
            $kec = $model->get_by([
                'code' => "{$village->province}.{$village->district}.{$village->subdistrict}.0000"
            ]);

            $kab = $model->get_by([
                'code' => "{$village->province}.{$village->district}.00.0000"
            ]);

            $pro = $model->get_by([
                'code' => "{$village->province}.00.00.0000"
            ]);
        ?>
        <tr onclick="$(this).find('input').prop('checked', 1);">
            <td><input type="radio" value="<?php echo $village->id ?>" name="location_id"></td>
            <td><?php echo $village->name ?></td>
            <td><?php echo @$kec->name ?></td>
            <td><?php echo @$kab->name ?></td>
            <td><?php echo @$pro->name ?></td>
        </tr>
        <?php endforeach ?>
    </tbody>
</table>

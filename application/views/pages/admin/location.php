<?php if(!defined('BASEPATH')) exit('No direct script access allowed!'); ?>
<pre class="hidden"><?php //print_r($submit)?></pre>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <i class="ion ion-clipboard"></i>
                    <h3 class="box-title"><?php echo $tagline; ?></h3>
                </div>
                <div class="box-body">
                    <?php if(isset($success)): ?>
                    <div class="alert alert-success">
                        <?php echo $success; ?>
                    </div>
                    <?php endif; ?>
                    <?php if(isset($error)): ?>
                    <div class="alert alert-success">
                        <?php echo $error; ?>
                    </div>
                    <?php endif; ?>
                    <?php echo form_open(current_url(), 'role="form" id="filter_form"') ?>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                <?php echo form_label('Provinsi'); ?>
                                <?php 
                                    $opt_province = array("0"=>"Pilih Provinsi");
                                    foreach ($province as $key => $row) {
                                        $opt_province[$row->province]=$row->name;
                                    }
                                    echo form_dropdown('province', $opt_province,array(@$submit['province']),'class="form-control"');
                                ?>
                                </div>
                                <div class="form-group">
                                <?php echo form_label('Kabupaten'); ?>
                                <?php 
                                    $opt_district = array("0"=>"Pilih Kabupaten");
                                    foreach ($district as $key => $row) {
                                        $opt_district[$row->district]=$row->name;
                                    }
                                    echo form_dropdown('district', $opt_district,array(@$submit['district']),'class="form-control"');
                                ?>
                                </div>
                                <div class="form-group">
                                <?php echo form_label('Kecamatan'); ?>
                                <?php 
                                    $opt_subdistrict = array("0"=>"Pilih Kecamatan");
                                    foreach ($subdistrict as $key => $row) {
                                        $opt_subdistrict[$row->subdistrict]=$row->name;
                                    }
                                    echo form_dropdown('subdistrict', $opt_subdistrict,array(@$submit['subdistrict']),'class="form-control"');
                                ?>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Cari</button>
                                </div>
                            </div>
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-4">
                            </div>
                            
                            
                            
                        </div>
                        </form>
                    <table class="table data-table">
                    	<thead>
                    		<tr>
                    			<th>Kode</th>
                                <th>Nama Lokasi</th>
                    			<th>Tingkat</th>
                    			<th>Actions</th>
                    		</tr>
                    	</thead>
                    	<tbody>
                        
                    	<?php foreach(@$loc as $row): ?>
                    		<tr>
                    			<td><?php echo $row->code; ?></td>
                                <td><?php echo $row->name; ?></td>
                    			<td>
                                    <?php 
                                    if(substr($row->code,-4)!="0000"){
                                        echo "Desa / Kelurahan";
                                    }elseif(substr($row->code,-7)!="00.0000"){
                                        echo "Kecamatan";
                                    }elseif(substr($row->code,-10)!="00.00.0000"){
                                        echo "Kabupaten / Kota";
                                    }
                                    ?>
                                </td>
                    			<td>
                    				<a href="<?php echo admin_url('location/form/'.$row->id); ?>" class="btn btn-xs btn-info">Edit</a>
                    			</td>
                    		</tr>
                    	<?php endforeach; ?>
                    	</tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
	$(function(){
		$('.data-table').dataTable();

        $("[name=province],[name=district],[name=subdistrict]").change(function(){
            $("#filter_form").submit();
        });

	})
</script>

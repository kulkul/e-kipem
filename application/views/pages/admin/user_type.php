<?php if(!defined('BASEPATH')) exit('No direct script access allowed!'); ?>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <i class="ion ion-clipboard"></i>
                    <h3 class="box-title"><?php echo $tagline; ?></h3>
                </div>
                <div class="box-body">
                    <?php if(isset($success)): ?>
                    <div class="alert alert-success">
                        <?php echo $success; ?>
                    </div>
                    <?php endif; ?>
                    <?php if(isset($error)): ?>
                    <div class="alert alert-success">
                        <?php echo $error; ?>
                    </div>
                    <?php endif; ?>
                    <table class="table data-table">
                    	<thead>
                    		<tr>
                    			<th>Nama Tipe</th>
                                <th>Keterangan</th>
                    		</tr>
                    	</thead>
                    	<tbody>
                    	<?php foreach($user_types as $row): ?>
                    		<tr>
                    			<td><?php echo $row->name; ?></td>
                                <td><?php echo $row->description; ?></td>
                    		</tr>
                    	<?php endforeach; ?>
                    	</tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    
	$(function(){
		$('.data-table').dataTable();
	})
</script>

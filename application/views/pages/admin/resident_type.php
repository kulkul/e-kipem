<?php if(!defined('BASEPATH')) exit('No direct script access allowed!'); ?>
<pre><?php //print_r($submit)?></pre>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <i class="ion ion-clipboard"></i>
                    <h3 class="box-title"><?php echo $tagline; ?></h3>
                </div>
                <div class="box-body">
                    <?php if(isset($success)): ?>
                    <div class="alert alert-success">
                        <?php echo $success; ?>
                    </div>
                    <?php endif; ?>
                    <?php if(isset($error)): ?>
                    <div class="alert alert-success">
                        <?php echo $error; ?>
                    </div>
                    <?php endif; ?>
                    <table class="table data-table">
                    	<thead>
                    		<tr>
                    			<th>Nama Tipe</th>
                                <th>Keterangan</th>
                    			<th>Actions</th>
                    		</tr>
                    	</thead>
                    	<tbody>
                    	<?php foreach($resident_types as $row): ?>
                    		<tr>
                    			<td><?php echo $row->name; ?></td>
                                <td><?php echo $row->description; ?></td>
                    			<td>
                    				<a href="<?php echo admin_url('resident_type/form/'.$row->id); ?>" class="btn btn-xs btn-info">Edit</a>
                                    <a href="<?php echo admin_url('resident_type/delete/'.$row->id); ?>" class="btn btn-xs btn-danger delete">Hapus</a>
                    			</td>
                    		</tr>
                    	<?php endforeach; ?>
                    	</tbody>
                    </table>
                </div>
                <div class="box-footer clearfix">
                    <a href="<?php echo admin_url('resident_type/form'); ?>" class="btn btn-sm btn-info">Tambah Tipe</a>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    
	$(function(){
		$('.data-table').dataTable();

        $("[name=province],[name=district],[name=subdistrict]").change(function(){
            $("#filter_form").submit();
        });

        $(".btn.delete").click(function(e){
            e.preventDefault();
            var x=confirm("Klik ok untuk menghapus.");
            if(x){
                document.location=$(this).attr("href");
            }
        });
	})
</script>

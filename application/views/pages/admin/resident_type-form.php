<?php defined('BASEPATH') or die('No direct script access allowed!'); ?>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <i class="ion ion-clipboard"></i>
                    <h3 class="box-title">Form Tipe Penduduk</h3>
                </div>
                <div class="box-body">
                    <?php if(validation_errors()): ?>
                    <div class="alert alert-danger">
                        <?php echo validation_errors() ?>
                    </div>
                    <?php endif; ?>
                    <?php echo form_open(current_url(), 'role="form"') ?>
                        <?php
                            echo form_hidden('id', set_value('id', @$resident_type->id));
                        ?>
                        <div class="form-group">
                            <?php echo form_label('Nama'); ?>
                            <?php
                                echo form_input(array(
                                        'type'  => 'text',
                                        'class' => 'form-control',
                                        'name'  => 'name',
                                        'value' => set_value('name', @$resident_type->name),
                                        'placeholder' => ''
                                    ));
                            ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Deskripsi'); ?>
                            <textarea name="description" class="form-control" rows="5"><?php echo set_value('description', @$resident_type->description); ?></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary btn-lg">Simpan</button>
                    </form>
                </div>
                <div class="box-footer clearfix">

                </div>
            </div>
        </div>
    </div>
</section>

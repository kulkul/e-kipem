<?php if(!defined('BASEPATH')) exit('No direct script access allowed!'); ?>
<pre class="hidden"><?php echo @$submit["province"]."."
                                                .@$submit["district"]."."
                                                .@$submit["subdistrict"]."."
                                                .@$submit["village"]; ?></pre>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <i class="ion ion-clipboard"></i>
                    <h3 class="box-title"><?php echo $tagline; ?></h3>
                </div>
                <div class="box-body">
                    <?php if(isset($success)): ?>
                    <div class="alert alert-success">
                        <?php echo $success; ?>
                    </div>
                    <?php endif; ?>
                    <?php if(isset($error)): ?>
                    <div class="alert alert-success">
                        <?php echo $error; ?>
                    </div>
                    <?php endif; ?>
                    <?php echo form_open(current_url(), 'role="form" id="filter_form"') ?>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                <?php echo form_label('Provinsi'); ?>
                                <?php
                                    $opt_province = array("0"=>"Pilih Provinsi");
                                    foreach ($province as $key => $row) {
                                        $opt_province[$row->province]=$row->name;
                                    }
                                    echo form_dropdown('province', $opt_province,array(@$submit['province']),'class="form-control"');
                                ?>
                                </div>
                                <div class="form-group">
                                <?php echo form_label('Kabupaten'); ?>
                                <?php
                                    $opt_district = array("0"=>"Pilih Kabupaten");
                                    foreach ($district as $key => $row) {
                                        $opt_district[$row->district]=$row->name;
                                    }
                                    echo form_dropdown('district', $opt_district,array(@$submit['district']),'class="form-control"');
                                ?>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Cari</button>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                <?php echo form_label('Kecamatan'); ?>
                                <?php
                                    $opt_subdistrict = array("0"=>"Pilih Kecamatan");
                                    foreach ($subdistrict as $key => $row) {
                                        $opt_subdistrict[$row->subdistrict]=$row->name;
                                    }
                                    echo form_dropdown('subdistrict', $opt_subdistrict,array(@$submit['subdistrict']),'class="form-control"');
                                ?>
                                </div>
                                <div class="form-group">
                                <?php echo form_label('Desa / Kelurahan'); ?>
                                <?php
                                    $opt_village = array("0"=>"Pilih Desa / Kelurahan");
                                    foreach ($village as $key => $row) {
                                        $opt_village[$row->village]=$row->name;
                                    }
                                    echo form_dropdown('village', $opt_village,array(@$submit['village']),'class="form-control"');
                                ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                            </div>



                        </div>
                        </form>
                    <table class="table data-table">
                    	<thead>
                    		<tr>
                                <th>Nama</th>
                                <th>NIK</th>
                                <th>Tempat Lahir</th>
                                <th>Tanggal Lahir</th>
                                <th>Jenis Kelamin</th>
                                <th>Alamat Asal</th>
                                <th>Lokasi Asal</th>
                                <th>Agama</th>
                                <th>Status</th>
                                <th>Pekerjaan</th>
                                <th>Berlaku Hingga (KTP)</th>
                                <th>Kewarganegaraan</th>
                                <th>Alamat Sekarang</th>
                                <th>Lokasi Sekarang</th>
                                <th>Jenis</th>
                                <th>Validasi</th>
                                <th>Kedaluwarsa (KIPEM)</th>
                                <th></th>
                    		</tr>
                    	</thead>
                    	<tbody>

                    	<?php foreach($residents as $row): ?>
                    		<tr>
                    			<td><?php echo @$row->id; ?></td>
                                <td><?php echo @$row->name; ?></td>
                                <td><?php echo @$row->birthplace; ?></td>
                                <td><?php echo @$row->birthddate; ?></td>
                                <td><?php echo @$row->sex; ?></td>
                                <td><?php echo @$row->ori_address; ?></td>
                                <td><?php echo @$row->ori_location_id; ?></td>
                                <td><?php echo @$row->religion; ?></td>
                                <td><?php echo @$row->marital_status; ?></td>
                                <td><?php echo @$row->occupation; ?></td>
                                <td><?php echo @$row->valid_until; ?></td>
                                <td><?php echo @$row->nationality; ?></td>
                                <td><?php echo @$row->cur_address; ?></td>
                                <td><?php echo @$row->cur_location_id; ?></td>
                                <td><?php echo @$row->resident_type_id; ?></td>
                                <td><?php echo @$row->validity; ?></td>
                                <td><?php echo @$row->exp_date; ?></td>
                    			<td>
                    				<a href="<?php echo admin_url('location/form/'.$row->id); ?>" class="btn btn-xs btn-info">Edit</a>
                    			</td>
                    		</tr>
                    	<?php endforeach; ?>
                    	</tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
	$(function(){
		$('.data-table').dataTable();

        $("[name=province],[name=district],[name=subdistrict],[name=village]").change(function(){
            $("#filter_form").submit();
        });

	})
</script>

<?php defined('BASEPATH') or die('No direct script access allowed!'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo asset_url('plugins/datepicker/datepicker3.css'); ?>">
<script src="<?php echo asset_url('plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>
<script>
    $(function(){
        $('.datepicker-lahir').datepicker({
            format: "yyyy-mm-dd",
            language: "id",
            autoclose: true
        });
    });
</script>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <i class="ion ion-clipboard"></i>
                    <h3 class="box-title">Form Penduduk Pendatang</h3>
                </div>
                <div class="box-body">
                    <?php if(validation_errors()): ?>
                    <div class="alert alert-danger">
                        <?php echo validation_errors() ?>
                    </div>
                    <?php endif; ?>
                    <?php echo form_open(current_url(), 'role="form"') ?>
                        <?php
                            echo form_hidden('id', set_value('id', @$resident->id));
                        ?>
                        <div class="form-group">
                            <?php echo form_label('Nama'); ?>
                            <?php
                                echo form_input(array(
                                        'type'  => 'text',
                                        'class' => 'form-control',
                                        'name'  => 'name',
                                        'value' => set_value('name', @$resident->name),
                                        'placeholder' => 'Nama'
                                    ));
                            ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('NIK'); ?>
                            <?php
                                echo form_input(array(
                                        'type'  => 'text',
                                        'class' => 'form-control',
                                        'name'  => 'nik',
                                        'value' => set_value('nik', @$resident->nik),
                                        'placeholder' => 'NIK'
                                    ));
                            ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Tempat Lahir'); ?>
                            <?php
                                echo form_input(array(
                                        'type'  => 'text',
                                        'class' => 'form-control',
                                        'name'  => 'birthplace',
                                        'value' => set_value('birthplace', @$resident->birthplace),
                                        'placeholder' => 'Tempat Lahir'
                                    ));
                            ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Tanggal Lahir'); ?>
                            <?php
                                echo form_input(array(
                                        'type'  => 'text',
                                        'class' => 'form-control datepicker-lahir',
                                        'name'  => 'birthdate',
                                        'value' => set_value('birthdate', @$resident->birthdate),
                                        'placeholder' => 'Tempat Lahir'
                                    ));
                            ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Jenis Kelamin'); ?>
                            <?php echo form_dropdown('sex', $sexs,
                            set_value('sex', @$resident->sex),
                            array('class' => 'form-control')); ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Agama'); ?>
                            <?php echo form_dropdown('religion', $religions,
                            set_value('religion', @$resident->religion),
                            array('class' => 'form-control')); ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Status Menikah'); ?>
                            <?php echo form_dropdown('marital_status', $maritals,
                            set_value('marital_status', @$resident->marital_status),
                            array('class' => 'form-control')); ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Pekerjaan'); ?>
                            <?php
                                echo form_input(array(
                                        'type'  => 'text',
                                        'class' => 'form-control',
                                        'name'  => 'occupation',
                                        'value' => set_value('occupation', @$resident->occupation),
                                        'placeholder' => 'Pekerjaan'
                                    ));
                            ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Kewarganegaraan'); ?>
                            <?php
                                echo form_input(array(
                                        'type'  => 'text',
                                        'class' => 'form-control',
                                        'name'  => 'nationality',
                                        'value' => set_value('nationality', @$resident->nationality),
                                        'placeholder' => 'Kewarganegaraan'
                                    ));
                            ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Alamat Asal'); ?>
                            <?php
                                echo form_input(array(
                                        'type'  => 'text',
                                        'class' => 'form-control',
                                        'name'  => 'ori_address',
                                        'value' => set_value('ori_address', @$resident->ori_address),
                                        'placeholder' => 'Alamat Asal'
                                    ));
                            ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Desa Asal'); ?>
                            <?php
                                echo form_input(array(
                                        'type'  => 'hidden',
                                        'class' => 'form-control',
                                        'name'  => 'ori_location_code',
                                        'value' => set_value('ori_location_code', @$resident->ori_location_code),

                                    ));
                                echo form_input(array(
                                        'type'  => 'text',
                                        'class' => 'form-control',
                                        'name'  => 'search_ori',
                                        'value' => '',
                                        'placeholder' => 'Ketikkan desa asal',
                                        'data-holder'=>'ori',
                                        'data-input'=>'ori_location_code',
                                    ))
                            ?>
                            <div class="search ori">
                            </div>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Kewarganegaraan'); ?>
                            <?php
                                echo form_input(array(
                                        'type'  => 'text',
                                        'class' => 'form-control',
                                        'name'  => 'nationality',
                                        'value' => set_value('nationality', @$resident->nationality),
                                        'placeholder' => 'Kewarganegaraan'
                                    ));
                            ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Masa Berlaku KTP Sampai'); ?>
                            <?php
                                echo form_input(array(
                                        'type'  => 'text',
                                        'class' => 'form-control datepicker-lahir',
                                        'name'  => 'valid_until',
                                        'value' => set_value('valid_until', @$resident->valid_until),
                                        'placeholder' => 'Masa Berlaku KTP'
                                    ));
                            ?>
                        </div>
                        <hr>
                        <h2>Informasi Tempat Tinggal</h2>
                        <div class="form-group">
                            <?php echo form_label('Alamat Tempat Tinggal'); ?>
                            <?php
                                echo form_input(array(
                                        'type'  => 'text',
                                        'class' => 'form-control',
                                        'name'  => 'cur_address',
                                        'value' => set_value('cur_address', @$resident->cur_address),
                                        'placeholder' => 'Alamat Tempat Tinggal'
                                    ));
                            ?>

                        </div>
                        <div class="form-group">
                            <?php echo form_label('Desa Tempat Tinggal'); ?>
                            <?php
                                // echo form_input(array(
                                //         'type'  => 'text',
                                //         'class' => 'form-control',
                                //         'name'  => 'cur_location_code',
                                //         'value' => set_value('cur_location_code', @$resident->cur_location_code),
                                //         'placeholder' => 'Desa Tempat Tinggal'
                                //     ));
                            ?>
                             <?php
                                echo form_input(array(
                                        'type'  => 'hidden',
                                        'class' => 'form-control',
                                        'name'  => 'cur_location_code',
                                        'value' => set_value('cur_location_code', @$resident->ori_location_code),

                                    ));
                                echo form_input(array(
                                        'type'  => 'text',
                                        'class' => 'form-control',
                                        'name'  => 'search_cur',
                                        'value' => '',
                                        'placeholder' => 'Ketikkan lokasi sekarang',
                                        'data-holder'=>'cur',
                                        'data-input'=>'cur_location_code',
                                    ))
                            ?>
                            <div class="search cur">
                            </div>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Status Tempat Tinggal'); ?>
                            <?php
                                echo form_input(array(
                                        'type'  => 'text',
                                        'class' => 'form-control',
                                        'name'  => 'resident_type_id',
                                        'value' => set_value('resident_type_id', @$resident->resident_type_id),
                                        'placeholder' => 'Status Tempat Tinggal'
                                    ));
                            ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Kipem Status'); ?>
                            <?php
                                $valid_dd = array(
                                    '0' => 'Tidak Valid',
                                    '1' => 'Valid'
                                );
                                echo form_dropdown('validity', $valid_dd,
                                set_value('validity', @$resident->validity),
                                array('class' => 'form-control'));
                            ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Kipem berlaku sampai'); ?>
                            <?php
                                echo form_input(array(
                                        'type'  => 'text',
                                        'class' => 'form-control datepicker-lahir',
                                        'name'  => 'exp_date',
                                        'value' => set_value('exp_date', @$resident->exp_date),
                                        'placeholder' => ''
                                    ));
                            ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Photo'); ?>
                            <?php
                                echo form_input(array(
                                        'type'  => 'text',
                                        'class' => 'form-control',
                                        'name'  => 'photo',
                                        'value' => set_value('photo', @$resident->photo),
                                        'placeholder' => ''
                                    ));
                            ?>
                        </div>
                        <hr>
                        <h2>Informasi Penanggung Jawab / Pemilik Tempat Tinggal</h2>
                        <p class="help-block">
                            Kosongkan jika tempat itu milik Anda
                        </p>
                        <div class="form-group">
                            <?php echo form_label('Nama Pemilik'); ?>
                            <?php
                                echo form_input(array(
                                        'type'  => 'text',
                                        'class' => 'form-control',
                                        'name'  => 'pic',
                                        'value' => set_value('pic', @$resident->pic),
                                        'placeholder' => 'Nama Pemilik'
                                    ));
                            ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Alamat'); ?>
                            <?php
                                echo form_input(array(
                                        'type'  => 'text',
                                        'class' => 'form-control',
                                        'name'  => 'pic_address',
                                        'value' => set_value('pic_address', @$resident->pic_address),
                                        'placeholder' => 'Nama Pemilik'
                                    ));
                            ?>
                            <p class="help-block">
                                Kosongkan jika sama dengan alamat Anda
                            </p>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('No Telepon / HP'); ?>
                            <?php
                                echo form_input(array(
                                        'type'  => 'text',
                                        'class' => 'form-control',
                                        'name'  => 'pic_phone',
                                        'value' => set_value('pic_phone', @$resident->pic_phone),
                                        'placeholder' => 'No Telepon / HP'
                                    ));
                            ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Email'); ?>
                            <?php
                                echo form_input(array(
                                        'type'  => 'text',
                                        'class' => 'form-control',
                                        'name'  => 'pic_email',
                                        'value' => set_value('pic_email', @$resident->pic_email),
                                        'placeholder' => 'Email'
                                    ));
                            ?>
                        </div>
                        <button type="submit" class="btn btn-primary btn-lg">Simpan</button>
                    </form>
                </div>
                <div class="box-footer clearfix">

                </div>
            </div>
        </div>
    </div>
</section>
<style type="text/css">
    .search{
        position: absolute;
        padding: 10px;
        box-shadow: 2px 2px 2px rgba(0,0,0,.6);
        overflow: auto;
        background-color: #fff;
        display: none;
    }
</style>
<script type="text/javascript">
    function search(obj){
        $.ajax({
            dataType:"json",
            method:"POST",
            data:"search="+$(obj).val(),
            url:"<?php echo site_url("admin/location/search");?>",
            success:function(data){
                //console.log(data);
                var cont=$(".search."+$(obj).attr("data-holder"));
                cont.html("");
                var link="";
                $(data).each(function(index,row){
                    //console.log(row);
                    link+="<a href='#' data-code='"+row.code+"'>"+row.name+"</a><br/>";
                });
                //console.log(link);
                cont.html(link);
                cont.find("a").click(function(e){
                    //alert();
                    e.preventDefault();
                    cont.hide();
                    $("[name="+$(obj).attr('data-input')+"]").val($(this).attr("data-code"));
                    $(obj).val($(this).html());
                });
                }
            });
    }
    (function(){
        //console.log($);
        $("[name=search_ori],[name=search_cur]").keyup(function(){
            $(".search."+$(this).attr("data-holder")).show();
            search(this);
        });
    }());
</script>

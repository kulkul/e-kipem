<?php defined('BASEPATH') or die('No direct script access allowed!'); ?>
<script>
    $(function(){

        $('#desa-input').keyup(function(e){
            var src = $(this).val();
            if(src.length >= 3){
                $.post($(this).data('url'), 'desa=' + $(this).val(), function(data){
                    $('#desa-search-result').html(data);
                });
            }
        });

    });
</script>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <i class="ion ion-clipboard"></i>
                    <h3 class="box-title">User Form</h3>
                </div>
                <div class="box-body">
                    <?php if(validation_errors()): ?>
                    <div class="alert alert-danger">
                        <?php echo validation_errors() ?>
                    </div>
                    <?php endif; ?>
                    <?php echo form_open(current_url(), 'role="form"') ?>
                        <?php
                            echo form_hidden('id', set_value('id', @$user->id));
                        ?>
                        <div class="form-group">
                            <?php echo form_label('Nama'); ?>
                            <?php
                                echo form_input(array(
                                        'type'  => 'text',
                                        'class' => 'form-control',
                                        'name'  => 'name',
                                        'value' => set_value('name', @$user->name),
                                        'placeholder' => 'Nama'
                                    ));
                            ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Alamat Lengkap'); ?>
                            <?php
                                echo form_input(array(
                                        'type'  => 'text',
                                        'class' => 'form-control',
                                        'name'  => 'address',
                                        'value' => set_value('address', @$user->address),
                                        'placeholder' => 'Alamat'
                                    ));
                            ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Email'); ?>
                            <?php
                                echo form_input(array(
                                        'type'  => 'email',
                                        'class' => 'form-control',
                                        'name'  => 'email',
                                        'value' => set_value('email', @$user->email),
                                        'placeholder' => 'Email'
                                    ));
                            ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Telepon / HP'); ?>
                            <?php
                                echo form_input(array(
                                        'type'  => 'text',
                                        'class' => 'form-control',
                                        'name'  => 'phone',
                                        'value' => set_value('phone', @$user->phone),
                                        'placeholder' => 'Telepon / HP'
                                    ));
                            ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('User Tipe'); ?>
                            <?php
                                echo form_dropdown('user_type_id',
                                        $user_type_dd,
                                        set_value('user_type_id', @$user->user_type_id),
                                        'class="form-control"');
                            ?>
                        </div>
                        <div>
                            <div class="form-group">
                                <?php echo form_label('Desa'); ?>
                                <p class="help-block">Pencarian desa.</p>
                                <input id="desa-input" class="form-control" data-url="<?php echo site_url('admin/user/search_desa') ?>">
                                <p class="help-block">Pilih salah satu desa di bawah ini.</p>
                            </div>
                            <div id="desa-search-result">

                            </div>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Password'); ?>
                            <?php
                                echo form_password(array(
                                        'class' => 'form-control',
                                        'name'  => 'password',
                                        'placeholder' => 'Password',
                                        'autocomplete' => 'off'
                                    ));
                            ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Keterangan'); ?>
                            <textarea name="note" class="form-control" rows="5"><?php echo set_value('note', @$user->note); ?></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary btn-lg">Submit</button>
                    </form>
                </div>
                <div class="box-footer clearfix">

                </div>
            </div>
        </div>
    </div>
</section>

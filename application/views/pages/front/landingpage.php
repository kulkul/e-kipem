    <!--===============================-->
    <!--== Langkah Mudah ===================-->
    <!--===============================-->
    <section id="langkahmudah" class="row">
      <div class="col-md-3 col-xs-6">
        <div class="event-info">
          <div class="icon fa fa-pencil-square-o"></div>
          <div class="info">
            <h3>Isi Data</h3>
            <span>Lengkapi permohonan</span>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-xs-6">
        <div class="event-info">
          <div class="icon fa fa-phone"></div>
          <div class="info">
            <h3>Konfirmasi</h3>
            <span>Petugas Menelepon</span>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-xs-6">
        <div class="event-info">
          <div class="icon fa fa-check-square-o"></div>
          <div class="info">
            <h3>Validasi</h3>
            <span>Petugas memvalidasi</span>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-xs-6">
        <div class="event-info">
          <div class="icon fa fa-credit-card"></div>
          <div class="info">
            <h3>KIPEM</h3>
            <span>Kartu Dikeluarkan</span>
          </div>
        </div>
      </div>
    </section>
    <!--==========-->



    <!--===============================-->
    <!--== Pengenalan ===================-->
    <!--===============================-->
    <section id="pengenalan" class="row">
      <div class="col-md-7 col-sm-12">
        <div class="content-tabs">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab1" data-toggle="tab">Syarat</a></li>
            <li><a href="#tab2" data-toggle="tab">Ketentuan</a></li>
          </ul>

          <div class="tab-content">
            <div class="tab-pane active" id="tab1">
              <div class="tab-body">
                <ol>
                    <li>SKKB / SKCK (SURAT KELAKUAN BAIK) DARI POLRI.</li>
                    <li>AKTE KELAHIRAN.</li>
                    <li>SURAT KETERANGAN BURUH KERJA / SURAT JALAN DARI DAERAH ASAL DIKETAHUI OLEH CAMAT SETEMPAT.</li>
                    <li>SURAT PENGANTAR DARI RT/RW DAERAH ASAL.</li>
                    <li>SURAT PERNYATAAN DARI PENAMPUNG ATAU PEMILIK TANAH YANG DIKETAHUI OLEH BENDESA ADAT / KADUS / KALING.</li>
                    <li>PAS FOTO UKURAN 3 X 4 LBR SEBANYAK 2 LBR.</li>
                </ol>
              </div>
            </div>
            <div class="tab-pane" id="tab2">
              <div class="tab-body">
                <ul>
                    <li>Bila sudah memiliki KTP sesuai dengan daerah tempat tinggal sekarang, pemohonan KIPEM tidak diperlukan.</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-5 col-sm-12">
        <h2>Mengapa e-Kipem?</h2>
        <p>Dengan menawarkan kemudahan dapat diakses dimana saja, e-Kipem (Kartu Ijin Penduduk Musiman) akan memberikan data yang relevan dan dapat diintegrasi ke data penduduk (KTP) untuk mengecek pencatatan ganda di masing-masing lingkungan. Serta, dapat membantu meminimalisir pembuatan kartu tanda penduduk (KTP) ganda.</p>
        <ul>
          <li>Mudah <span class="fa fa-check"></span></li>
          <li>Cepat <span class="fa fa-check"></span></li>
          <li>Transparan <span class="fa fa-check"></span></li>
        </ul>
      </div>
    </section>
    <!--==========-->


    <!--===============================-->
    <!--== Cek Status Form =============-->
    <!--===============================-->
    <section id="cekstatus" class="row">
      <div class="col-sm-12">
        <div class="section-header text-left">
          <h2>Cek Status</h2>
          <h4>Masukkan NIK yang tertera pada KTP Anda.</h4>
        </div>
      </div>
      <div class="col-sm-12">
        <form id="cekstatuskipem-form" method="post" class="form" action="<?php echo site_url("resident/get");?>">
          <div class="col-sm-9 col-xs-12">
            <input name="nik" id="nik" type="text" placeholder="Nomor Induk KTP" required>
          </div>
          <div class="col-sm-3 col-xs-12">
            <input type="submit" value="Cek Status" name="submit">
          </div>
          <p class="form-notification" style="display: none;">Status</p>
        </form>
      </div>
    </section>
    <!--==========-->


    <!--===============================-->
    <!--== Kepala Lingkungan ==========-->
    <!--===============================-->
    <!--<section id="partners" class="row">
      <div class="col-sm-12">
        <div class="section-header text-center">
          <h2>Kepala Lingkungan</h2>
          <h4>Kenali kepala lingkungan yang akan membantu Anda.</h4>
        </div>
      </div>
      <div class="col-sm-12">
        <div id="carousel" class="owl-carousel">
          <div class="item"><img src="<?php echo base_url("/assets/images/slider/partners/logo-01.png");?>" alt="Logo 1"></div>
          <div class="item"><img src="<?php echo base_url("/assets/images/slider/partners/logo-02.png");?>" alt="Logo 2"></div>
          <div class="item"><img src="<?php echo base_url("/assets/images/slider/partners/logo-03.png");?>" alt="Logo 3"></div>
          <div class="item"><img src="<?php echo base_url("/assets/images/slider/partners/logo-04.png");?>" alt="Logo 4"></div>
          <div class="item"><img src="<?php echo base_url("/assets/images/slider/partners/logo-05.png");?>" alt="Logo 5"></div>
          <div class="item"><img src="<?php echo base_url("/assets/images/slider/partners/logo-06.png");?>" alt="Logo 6"></div>
        </div>
        <div class="owl-carousel-navigation">
          <a class="owl-btn prev fa fa-angle-left"></a>
          <a class="owl-btn next fa fa-angle-right"></a>
        </div>
      </div>
    </section>-->
    <!--==========-->


    <!--===============================-->
    <!--== Daftar e-Kipem =============-->
    <!--===============================-->
    <section id="register" class="row">
      <div class="col-sm-12">
        <div class="section-header text-center">
          <h2>Daftar e-Kipem</h2>
          <h4>Ayo buat data kependudukan sementara Anda.</h4>
        </div>
      </div>
      <div class="col-sm-12">
        <form id="register-form" method="post" class="form">
          <div class="col-md-2 col-sm-6">
            <input name="name" id="name" type="text" placeholder="Nama Lengkap" required>
          </div>
          <div class="col-md-4 col-sm-6">
            <input name="address" id="address" type="text" placeholder="Alamat Sekarang" required>
          </div>
          <div class="col-md-2 col-sm-6">
            <input name="telephone" id="telephone" type="tel" placeholder="No. Telepon Aktif">
          </div>
          <div class="col-md-2 col-sm-6">
            <select name="ticket" id="ticket">
              <option value="1">Pria</option>
              <option value="0">Wanita</option>
            </select>
          </div>
          <div class="col-md-2 col-sm-12">
            <input type="submit" value="Kirim" name="submit">
          </div>
          <p class="form-notification" style="display: none;">Registration Complete</p>
        </form>
      </div>
    </section>
    <!--==========-->
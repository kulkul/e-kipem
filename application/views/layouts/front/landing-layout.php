<?php if(!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<!doctype html>
<html>
<head>
    <!-- robot speak -->    
    <meta charset="utf-8">
    <title><?php echo $title; ?></title>
    <?php echo chrome_frame(); ?>
    <?php echo view_port(); ?>
    <?php echo $meta; ?>
    <?php echo $css; ?>
</head>
<body>

    <header id="header">
      <?php echo $header; ?>
    </header>

    <div class="content">
      <div class="container box">


        <?php echo $content; ?>

        <div id="gotop" class="gotop fa fa-arrow-up"></div>
      </div>
    </div>

    <footer id="footer">
      <?php echo $footer; ?>
    </footer>
    <?php echo $js; ?>
</body>
</html>
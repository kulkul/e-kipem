<?php
if(!defined('BASEPATH')) exit('No direct script access allowed!');

class Resident extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		/* load model */
		$this->load->model(array('resident_model','user_model','location_model','resident_type_model'));
	}

	public function get(){
		//print_r($this->input->post());
		$res=$this->resident_model->get_by(array("nik"=>$this->input->post("nik")));
		//print_r($res);
		if(!is_object($res)){
			echo "KTP anda belum terdaftar E-Kipem";
		}elseif($res->validity==0){
			echo "KTP anda sedang dalam proses validasi E-Kipem";
		}else{
			echo "Anda sudah terdaftar E-Kipem";
		}

	}

}

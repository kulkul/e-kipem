<?php

class Register extends Front_Controller {

    public function __construct () {
        parent::__construct();
        $this->load->model([
            'location_model'
        ]);
    }

    public function index () {

        if ( $this->input->method() == 'get') {
            $this->_form();
        } elseif ( $this->input->method() == 'post' ) {

        } else {
            show_404();
        }

    }

    private function _save () {

    }

    private function _form () {
        $this->stencil->paint('register');
    }
}

<?php
if(!defined('BASEPATH')) exit('No direct script access allowed!');

class User extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();

		/* load model */
		$this->load->model(array(
			'user_model',
			'location_model',
			'user_type_model'
		));
	}

	public function index()
	{
		$this->stencil->css(
			asset_url('plugins/datatables/dataTables.bootstrap.css'));

		$this->stencil->js(array(
			asset_url('plugins/datatables/jquery.dataTables.js'),
			asset_url('plugins/datatables/dataTables.bootstrap.js'),
			asset_url('plugins/slimScroll/jquery.slimscroll.min.js')
		));

		$data['users'] = $this->user_model->with('location')
							->with('user_type')
							->get_all();
		$data['tagline'] = 'Manage all users';

		$this->stencil->title('User Management');
        $this->stencil->data($this->session->flashdata());
		$this->stencil->data($data);

		$this->stencil->paint('admin/user');
	}

	public function form($id = NULL)
	{
        $data = array();

		$data['user_type_dd'] = $this->user_type_model->dropdown('id', 'name');

		if($id == NULL)
		{
			$this->stencil->title('Add New User');
		}else{
			$this->stencil->title('Update User');
			$data['user'] = $this->user_model->get($id);
		}

        $this->stencil->data($data);

		if($this->input->method() != 'post')
		{
			$this->stencil->paint('admin/user-form');
		}else{
			$this->load->library('form_validation');

			$this->form_validation->set_rules('id', 'User ID', 'integer');
			$this->form_validation->set_rules('name', 'Name', 'required');

			if($id == NULL )
                $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[user.email]');
			if($id == NULL )
                $this->form_validation->set_rules('password', 'Password', 'required');
			if($id == NULL )
				$this->form_validation->set_rules('location_id', 'Village', 'required');

			$this->form_validation->set_rules('user_type_id', 'User Type', 'required');
			$this->form_validation->set_rules('address', 'Address', 'required');
			$this->form_validation->set_rules('phone', 'Phone', 'required');
			$this->form_validation->set_rules('note', 'Note');

			if($this->form_validation->run() === FALSE)
			{
				$this->stencil->paint('admin/user-form');
			}else{
				$values = array(
					'name'			=> $this->input->post('name'),
					'email'			=> $this->input->post('email'),
					'user_type_id'	=> $this->input->post('user_type_id'),
					'location_id'	=> $this->input->post('location_id'),
					'address'		=> $this->input->post('address'),
					'phone'			=> $this->input->post('phone'),
					'note'			=> $this->input->post('note'),
					'password'		=> $this->input->post('password')
					);

				if($this->input->post('id') == '')
				{
					$this->user_model->insert($values);
					$this->session->set_flashdata('success', 'New user has been added.');
				}else{
				    if($values['password'] == '') unset($values['password']);
					if($values['location_id'] == '') unset($values['location_id']);
					$this->user_model->update($this->input->post('id'), $values);
					$this->session->set_flashdata('success', 'User has been updated.');
				}
				redirect('admin/user');
			}
		}
	}

    public function deactivate($id)
    {
        $values = array(
			'active' 	=> 0,
		);
        $this->user_model->update($this->input->post('id'), $values);
		$this->session->set_flashdata('success', 'User has been deactivated.');
        redirect('admin/user');
    }

	public function search_desa() {
		$this->db->like('name', $this->input->post('desa'), 'after');
		$this->db->where('village != ', '0000');
		$data['villages'] = $this->location_model->limit(25)
							->get_all();
		$data['model'] = $this->location_model;
		$this->load->view('pages/admin/user-search-desa', $data);
	}

    public function send_email($id)
    {
        $data = array();
        $this->stencil->title('Send email');

		if($id != NULL)
		{
			$data['user'] = $this->user_model->get($id);
		}

        $this->stencil->data($data);

        if($this->input->method() != 'post')
        {
            $this->stencil->js(asset_url('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js'));
            $this->stencil->css(asset_url('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css'));
            $this->stencil->paint('admin/user-send-email');
        }else{
            $user = $this->kulkul_auth->user();

            $this->load->library('email');

            $config['charset'] = 'iso-8859-1';
            $config['wordwrap'] = TRUE;
            $config['mailtype'] = 'html';

            $this->email->initialize($config);

            $this->email->from($user->email, $user->full_name);
            $this->email->to($this->input->post('to'));
            $this->email->cc($this->input->post('cc'));
            $this->email->bcc($this->input->post('bcc'));

            $this->email->subject($this->input->post('subject'));
            $this->email->message($this->input->post('body'));

            $this->email->send();

            $this->session->set_flashdata('success', 'Your email has been sent');
            redirect('admin/user');
        }
    }
}

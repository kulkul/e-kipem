<?php
if(!defined('BASEPATH')) exit('No direct script access allowed!');

class User_type extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();

		/* load model */
		$this->load->model(array('user_model','location_model','user_type_model'));
	}

	public function index()
	{
		$this->stencil->css(
			asset_url('plugins/datatables/dataTables.bootstrap.css'));

		$this->stencil->js(array(
			asset_url('plugins/datatables/jquery.dataTables.js'),
			asset_url('plugins/datatables/dataTables.bootstrap.js'),
			asset_url('plugins/slimScroll/jquery.slimscroll.min.js')
		));

		$data['user_types'] = $this->user_type_model->get_all();
		
		$data['tagline'] = 'Data Tipe User';

		$this->stencil->title('Data Master');
        $this->stencil->data($this->session->flashdata());
		$this->stencil->data($data);

		$this->stencil->paint('admin/user_type');
	}




    

    
}

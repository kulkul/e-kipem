<?php
if(!defined('BASEPATH')) exit('No direct script access allowed!');

class Resident extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();

		/* load model */
		$this->load->model(array('resident_model','user_model','location_model','resident_type_model'));
	}

	public function index()
	{
		$this->stencil->css(
			asset_url('plugins/datatables/dataTables.bootstrap.css'));

		$this->stencil->js(array(
			asset_url('plugins/datatables/jquery.dataTables.js'),
			asset_url('plugins/datatables/dataTables.bootstrap.js'),
			asset_url('plugins/slimScroll/jquery.slimscroll.min.js')
		));


		$data['province'] = $this->location_model->get_many_by(array("district"=>"00"));
		$data['district'] = array();
		$data['subdistrict'] = array();
		$data['village'] = array();
		$search=array();
		if($this->input->post()){
			$data['province'] = $this->location_model->get_many_by(array("district"=>"00"));
			$data['district'] = $this->location_model->get_many_by(array("province"=>$this->input->post("province"),"`district`>00","subdistrict"=>"00","village"=>"0000"));
			$data['subdistrict'] = $this->location_model->get_many_by(array("province"=>$this->input->post("province"),"district"=>$this->input->post("district"),"`subdistrict`>00","village"=>"0000"));
			$data['village'] = $this->location_model->get_many_by(array("province"=>$this->input->post("province"),"district"=>$this->input->post("district"),"subdistrict"=>$this->input->post("subdistrict"),"`village`>0"));
			//echo $this->db->last_query();
			$data['submit']=$this->input->post();
			$post=$this->input->post();


			if($post["province"]>0){
				$search=array("LEFT(`cur_location_code`,2)='".$post["province"]."'");
			}
			if($post["district"]>0){
				$search=array("LEFT(`cur_location_code`,5)='".$post["province"].".".$post["district"]."'");
			}
			if($post["subdistrict"]>0){
				$search=array("LEFT(`cur_location_code`,8)='".$post["province"]."."
															.$post["district"]."."
															.$post["subdistrict"]."'");
			}
			if($post["village"]>0){
				$search=array("cur_location_code"=>$post["province"]."."
												.$post["district"]."."
												.$post["subdistrict"]."."
												.$post["village"]);
			}


		}
		$data['residents'] = $this->resident_model->get_many_by($search);

		$data['tagline'] = 'Penduduk';

		$this->stencil->title('Data');
        $this->stencil->data($this->session->flashdata());
		$this->stencil->data($data);

		$this->stencil->paint('admin/resident');
	}

	public function form($id = NULL)
	{
        $data = array();

		if($id == NULL)
		{
			$this->stencil->title('Data Master');
		}else{
			$this->stencil->title('Form Penduduk Pendatang');
			$data['resident_type'] = $this->resident_type_model->get($id);
		}

		$data['sexs'] = $this->resident_model->sex_dropdown();
		$data['religions'] = $this->resident_model->religion_dropdown();
		$data['maritals'] = $this->resident_model->marital_dropdown();

        $this->stencil->data($data);

		if($this->input->method() != 'post')
		{
			$this->stencil->paint('admin/resident-form');
		}else{
			$this->load->library('resident_lib');

			$data_penduduk = array(
			    'name'          => $this->input->post('name'),
			    'nik'           => $this->input->post('nik'),
			    'birthplace'    => $this->input->post('birthplace'),
			    'birthdate'     => $this->input->post('birthdate'),
			    'sex'           => $this->input->post('sex'),
			    'ori_address'   => $this->input->post('ori_address'),
			    'ori_location_code' => $this->input->post('ori_location_code'),
			    'religion'      => $this->input->post('religion'),
			    'marital_status' => $this->input->post('marital_status'),
			    'occupation'    => $this->input->post('occupation'),
			    'valid_until'   => $this->input->post('valid_until'),
			    'nationality'   => $this->input->post('nationality'),
			    'cur_address'   => $this->input->post('cur_address'),
			    'cur_location_code'  => $this->input->post('cur_location_code'),
			    'resident_type_id' => $this->input->post('resident_type_id'),
			    'validity'      => $this->input->post('validity'),
			    'exp_date'      => $this->input->post('exp_date'),
			    'photo'         => $this->input->post('photo'),
			    'pic'           => $this->input->post('pic'),
			    'pic_address'   => $this->input->post('pic_address'),
			    'pic_phone'     => $this->input->post('pic_phone'),
			    'pic_email'     => $this->input->post('pic_email')
			);

			if($this->input->post('id') == '') {
				$insert = $this->resident_lib->insert($data_penduduk);
			} else {
				$insert = $this->resident_lib->update($this->input->post('id'), $data_penduduk);
			}

			if (!$insert) {
				$this->stencil->data('errors', $this->resident_lib->validation_errors());
				$this->stencil->paint('admin/resident-form');
			} else {
				if($this->input->post('id') == '')
				{
					$this->session->set_flashdata('success', 'Data tipe penduduk baru ditambahkan.');
				}else{
					$this->session->set_flashdata('success', 'Data tipe penduduk diperbaharui.');
				}
				redirect('admin/resident');
			}
		}
	}

	public function delete($id=null){
		$this->resident_type_model->delete($id);
		redirect(site_url("admin/resident_type"));
	}

	private function _upload () {
		
		$config['upload_path']          = FCPATH.'assets/uploads/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 2048;
        $config['max_width']            = 10240;
        $config['max_height']           = 7680;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('foto'))
        {
            $error = array('error' => $this->upload->display_errors());
			return $error;
        }
        else
        {
			$data = array('upload_data' => $this->upload->data());
            return $data;
        }
	}


}

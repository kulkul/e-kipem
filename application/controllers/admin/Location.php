<?php
if(!defined('BASEPATH')) exit('No direct script access allowed!');

class Location extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();

		/* load model */
		$this->load->model(array('user_model','location_model'));
	}

	public function index()
	{
		$this->stencil->css(
			asset_url('plugins/datatables/dataTables.bootstrap.css'));

		$this->stencil->js(array(
			asset_url('plugins/datatables/jquery.dataTables.js'),
			asset_url('plugins/datatables/dataTables.bootstrap.js'),
			asset_url('plugins/slimScroll/jquery.slimscroll.min.js')
		));

		$data['province'] = $this->location_model->get_many_by(array("district"=>"00"));
		$data['district'] = array();
		$data['subdistrict'] = array();
		$data['village'] = array();
		$data['loc']=array();
		if($this->input->post()){
			$post=$this->input->post();
			$data['province'] = $this->location_model->get_many_by(array("district"=>"00"));
			$data['district'] = $this->location_model->get_many_by(array("province"=>$post["province"],"`district`>00","subdistrict"=>"00","village"=>"0000"));
			$data['subdistrict'] = $this->location_model->get_many_by(array("province"=>$post["province"],"district"=>$post["district"],"`subdistrict`>00","village"=>"0000"));
			
			$search=array();
			if($post['district']>0){
				$search["province"]=$post["province"];	
				$search["district"]=$post["district"];	
			}
			if($post['subdistrict']>0){
				$search["subdistrict"]=$post["subdistrict"];	
				
			}
			if(count($search)>0){
				$data['loc'] = $this->location_model->get_many_by($search);	
			}else{
				$data['loc']=array();
			}
			
			//echo $this->db->last_query();
			$data['submit']=$this->input->post();
		}


		$data['tagline'] = 'Data Semua Lokasi';

		$this->stencil->title('Data Lokasi');
        $this->stencil->data($this->session->flashdata());
		$this->stencil->data($data);

		$this->stencil->paint('admin/location');
	}

	
	public function search($search=""){
		$this->db->like("name",@$this->input->post("search"));
		//$this->db->like("name",$search);
		$this->db->where("`village`>'00'",null,false);
		$res = $this->location_model->limit(10)->get_all();		
		echo json_encode($res);
		// echo $this->db->last_query();
		//echo json_encode(array($this->db->last_query()));
	}
    

    
}

<?php
if(!defined('BASEPATH')) exit('No direct script access allowed!');

class Resident_type extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();

		/* load model */
		$this->load->model(array('user_model','location_model','resident_type_model'));
	}

	public function index()
	{
		$this->stencil->css(
			asset_url('plugins/datatables/dataTables.bootstrap.css'));

		$this->stencil->js(array(
			asset_url('plugins/datatables/jquery.dataTables.js'),
			asset_url('plugins/datatables/dataTables.bootstrap.js'),
			asset_url('plugins/slimScroll/jquery.slimscroll.min.js')
		));

		$data['resident_types'] = $this->resident_type_model->get_all();
		
		$data['tagline'] = 'Data Tipe Penduduk';

		$this->stencil->title('Data Master');
        $this->stencil->data($this->session->flashdata());
		$this->stencil->data($data);

		$this->stencil->paint('admin/resident_type');
	}

	public function form($id = NULL)
	{
        $data = array();

		if($id == NULL)
		{
			$this->stencil->title('Data Master');
		}else{
			$this->stencil->title('Perbaharui User');
			$data['resident_type'] = $this->resident_type_model->get($id);
		}

        $this->stencil->data($data);

		if($this->input->method() != 'post')
		{
			$this->stencil->paint('admin/resident_type-form');
		}else{
			$this->load->library('form_validation');

			$this->form_validation->set_rules('id');
			$this->form_validation->set_rules('name', 'Nama Tipe', 'required');
			$this->form_validation->set_rules('description');

			if($this->form_validation->run() === FALSE)
			{
				$this->stencil->paint('admin/resident_type-form');
			}else{
				$values = array(
					'name'			=> $this->input->post('name'),
					'description'	=> $this->input->post('description'),
					);

				if($this->input->post('id') == '')
				{
					$this->resident_type_model->insert($values);
					$this->session->set_flashdata('success', 'Data tipe penduduk baru ditambahkan.');
				}else{
					$this->resident_type_model->update($this->input->post('id'), $values);
					$this->session->set_flashdata('success', 'Data tipe penduduk diperbaharui.');
				}
				redirect('admin/resident_type');
			}
		}
	}

	public function delete($id=null){
		$this->resident_type_model->delete($id);
		redirect(site_url("admin/resident_type"));
	}

    

    
}

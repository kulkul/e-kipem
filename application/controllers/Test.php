<?php

class Test extends Front_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array(
            'resident_lib'
        ));
    }

    public function index()
    {
        $data = array(
            'name' => '',
            'birthplace' => '',
            'birthdate' => '',
            'sex' => '',
            'ori_address' => '',
            'ori_location_id' => '',
            'religion' => '',
            'marital_status' => '',
            'occupation' => '',
            'valid_until' => '',
            'nationality' => '',
            'cur_address' => '',
            'cur_location_id' => '',
            'resident_type_id' => '',
            'validity' => '',
            'exp_date' => '',
            'photo' => '',
            'pic' => '',
            'pic_address' => '',
            'pic_phone' => '',
            'pic_email' => ''
        );

        $valid = $this->resident_lib->validate($data);

        var_dump($this->resident_lib->validation_errors());


    }
}
